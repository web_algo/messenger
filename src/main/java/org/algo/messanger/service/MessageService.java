package org.algo.messanger.service;

import org.algo.messanger.model.response.MessageResponseDto;
import org.algo.messanger.model.response.template.ApiResponse;
import org.springframework.http.ResponseEntity;

public interface MessageService {
    ResponseEntity<ApiResponse<MessageResponseDto>> getMessage(Long id);
}
