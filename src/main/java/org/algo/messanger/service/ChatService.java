package org.algo.messanger.service;

import org.algo.messanger.model.request.ChatPermissionDto;
import org.algo.messanger.model.response.ChatResponseDto;
import org.algo.messanger.model.response.MessageResponseDto;
import org.algo.messanger.model.response.template.ApiResponse;
import org.algo.messanger.model.response.template.ListPage;
import org.springframework.http.ResponseEntity;

public interface ChatService {
    ResponseEntity<ApiResponse<ChatResponseDto>> get(Long chatId);

    ResponseEntity<ApiResponse<ChatResponseDto>> update(ChatPermissionDto chatPermissionDto, Long chatId);

    ResponseEntity<ApiResponse<ListPage<MessageResponseDto>>> getMessages(Long start, Integer limit, Long chatId);

    ResponseEntity<ApiResponse<ChatResponseDto>> create(Long myUserId, String interlocutorUsername);
}
