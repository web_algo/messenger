package org.algo.messanger.service;

import org.algo.messanger.domain.VerificationEntity;
import org.algo.messanger.model.response.UserResponseDto;
import org.algo.messanger.model.response.template.ApiResponse;
import org.springframework.http.ResponseEntity;

public interface VerificationService {

    VerificationEntity sendVerificationToken(String toAddress);

    VerificationEntity sendVerificationCode(String toAddress);

    ResponseEntity<ApiResponse<UserResponseDto>> verifyEmail(String email, String code);

    ResponseEntity<ApiResponse<UserResponseDto>> verifyNewPassword(String email, String verificationCode);
}
