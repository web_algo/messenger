package org.algo.messanger.service.impl;

import lombok.RequiredArgsConstructor;
import org.algo.messanger.domain.sittings.*;
import org.algo.messanger.enums.AwayFor;
import org.algo.messanger.enums.Language;
import org.algo.messanger.repository.*;
import org.algo.messanger.service.SittingsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class SittingsServiceImpl implements SittingsService {

    private final SittingsRepo sittingsRepo;
    private final TwoStepAuthRepo twoStepAuthRepo;
    private final FolderSittingsRepo folderSittingsRepo;
    private final NotificationSittingsRepo notificationSittingsRepo;
    private final PrivacySecuritySittingsRepo privacySecuritySittingsRepo;


    public SittingsEntity getDefaultSittings() {

        FolderSittingsEntity folderSittingsEntity = new FolderSittingsEntity();
        folderSittingsEntity.setMyFolderList(new ArrayList<>());
        folderSittingsEntity = folderSittingsRepo.save(folderSittingsEntity);

        NotificationSittingsEntity notificationSittings = new NotificationSittingsEntity();
        notificationSittings.setJoinTelegram(true);
        notificationSittings.setPinnedMessage(true);
        notificationSittings.setPlaySound(true);
        notificationSittings.setShowSenderName(true);
        notificationSittings.setShowPreviewMessage(true);
        notificationSittings = notificationSittingsRepo.save(notificationSittings);


        PrivacySecuritySittingsEntity privacySecuritySittingsEntity = new PrivacySecuritySittingsEntity();
        privacySecuritySittingsEntity.setAddMeToGroupsOrChannels(true);
        privacySecuritySittingsEntity.setShowProfilePhoto(true);
        privacySecuritySittingsEntity.setShowPhone(true);
        privacySecuritySittingsEntity.setShowLastSeenOnline(true);
        privacySecuritySittingsEntity.setIfAwayFor(AwayFor.THREE_MONTH);

        TwoStepAuth twoStepAuth = new TwoStepAuth();
        twoStepAuth.setAuthActive(false);
        twoStepAuth = twoStepAuthRepo.save(twoStepAuth);

        privacySecuritySittingsEntity.setTwoStepAuth(twoStepAuth);
        privacySecuritySittingsEntity = privacySecuritySittingsRepo.save(privacySecuritySittingsEntity);


        SittingsEntity sittingsEntity = new SittingsEntity();

        sittingsEntity.setLanguage(Language.ENGLISH);
        sittingsEntity.setFolderSittings(folderSittingsEntity);
        sittingsEntity.setNotificationSittings(notificationSittings);
        sittingsEntity.setPrivacySecuritySittings(privacySecuritySittingsEntity);

        return sittingsRepo.save(sittingsEntity);
    }


}
