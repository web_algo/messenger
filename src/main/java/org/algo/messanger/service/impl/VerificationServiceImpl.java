package org.algo.messanger.service.impl;

import lombok.RequiredArgsConstructor;
import org.algo.messanger.common.Generate;
import org.algo.messanger.domain.PasswordTemporary;
import org.algo.messanger.domain.VerificationEntity;
import org.algo.messanger.domain.user.TemporaryUserEntity;
import org.algo.messanger.domain.user.UserEntity;
import org.algo.messanger.enums.Error;
import org.algo.messanger.model.response.UserResponseDto;
import org.algo.messanger.model.response.template.ApiResponse;
import org.algo.messanger.model.response.template.ErrorMessage;
import org.algo.messanger.model.response.template.SuccessMessage;
import org.algo.messanger.repository.PasswordTemporaryRepo;
import org.algo.messanger.repository.TemporaryRepo;
import org.algo.messanger.repository.UserRepo;
import org.algo.messanger.repository.VerificationRepo;
import org.algo.messanger.service.UserService;
import org.algo.messanger.service.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;

@Service
@Transactional
@RequiredArgsConstructor
public class VerificationServiceImpl implements VerificationService {

    private final UserRepo userRepo;
    private final JavaMailSender emailSender;
    private final VerificationRepo verificationRepo;
    private final TemporaryRepo temporaryRepo;
    private final PasswordTemporaryRepo passwordTemporaryRepo;
    @Autowired
    UserService userService;


    private final String verificationLinkTemplate = " " +
            "<h3 style='display: inline;'>Verification link is: </h3>\n";
    private final String verificationCodeTemplate = " " +
            "<h3 style='display: inline;'>Verification code is: </h3>\n";

    @Override
    public VerificationEntity sendVerificationToken(String toAddress) {

        //todo generate code
        String verificationCode = generateCode();

        //todo check verificationCode already exist
        while (verificationRepo.findByCode(verificationCode).isPresent()) {
            verificationCode = generateCode();
        }
        //todo create new object for db
        VerificationEntity verificationEntity = new VerificationEntity();
        verificationEntity.setEmail(toAddress);
        verificationEntity.setExpireDate(LocalDateTime.now().plusSeconds(VerificationEntity.termSeconds));
        verificationEntity.setCode(verificationCode);

        //todo check already exist if exist update
        Optional<VerificationEntity> optionalVerification = verificationRepo.findByEmail(toAddress);
        if (optionalVerification.isPresent()) {
            verificationEntity.setId(optionalVerification.get().getId());
        }


        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(toAddress);
            helper.setSubject("Verification");
            helper.setText(verificationLinkTemplate + "<p style='display: inline;' >http://localhost:8080/api/verify?email=" + toAddress + "&verificationCode=" + verificationCode + "</p>", true);
            emailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return verificationRepo.save(verificationEntity);
    }

    @Override
    public VerificationEntity sendVerificationCode(String toAddress) {

        //todo generate code
        String verificationCode = generateCode();

        //todo check verificationCode already exist
        while (verificationRepo.findByCode(verificationCode).isPresent()) {
            verificationCode = generateCode();
        }
        //todo create new object for db
        VerificationEntity verificationEntity = new VerificationEntity();
        verificationEntity.setEmail(toAddress);
        verificationEntity.setExpireDate(LocalDateTime.now().plusMinutes(VerificationEntity.termSeconds));
        verificationEntity.setCode(verificationCode);

        //todo check already exist if exist update
        Optional<VerificationEntity> optionalVerification = verificationRepo.findByEmail(toAddress);
        if (optionalVerification.isPresent()) {
            verificationEntity.setId(optionalVerification.get().getId());
        }


        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(toAddress);
            helper.setSubject("Verification");
            helper.setText(verificationCodeTemplate + "<p style='display: inline;' > verificationCode </p>", true);
            emailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return verificationRepo.save(verificationEntity);
    }

    @Override
    public ResponseEntity<ApiResponse<UserResponseDto>> verifyEmail(String email, String code) {

        //todo get temporary user
        Optional<TemporaryUserEntity> optionalTemporaryUser = temporaryRepo.findByEmail(email);
        if (optionalTemporaryUser.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage()), HttpStatus.NOT_FOUND);
        }


        TemporaryUserEntity temporaryUser = optionalTemporaryUser.get();

        //todo check email
        if (!temporaryUser.getVerification().getEmail().equals(email) ||
                !temporaryUser.getVerification().getCode().equals(code)
        ) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage()), HttpStatus.NOT_FOUND);
        }

        //todo check expire date verification code
        if (temporaryUser.getVerification().getExpireDate().isBefore(LocalDateTime.now())) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage(Error.EXPIRED.getErrorMessage(VerificationEntity.termSeconds))), HttpStatus.BAD_REQUEST);
        }


        UserEntity userEntity = userService.toUserEntity(temporaryUser);

        temporaryRepo.delete(temporaryUser);
        verificationRepo.delete(temporaryUser.getVerification());
        UserResponseDto userResponseDto = userService.toUserResponse(userEntity);

        return new ResponseEntity<>(new ApiResponse<>(userResponseDto, new SuccessMessage()), HttpStatus.OK);

    }


    private String generateCode() {
        Generate generate = new Generate(8, new Random(), Generate.alphanum);
        return generate.nextString();
    }

    @Override
    public ResponseEntity<ApiResponse<UserResponseDto>> verifyNewPassword(String email, String verificationCode) {
        Optional<PasswordTemporary> byEmail = passwordTemporaryRepo.findByEmail(email);
        if (byEmail.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage(Error.EMAIL_NOT_FOUND.getErrorMessage(email))), HttpStatus.NOT_FOUND);
        }

        PasswordTemporary passwordTemporary = byEmail.get();

        //todo check verification expire date
        if (passwordTemporary.getVerification().getExpireDate().isBefore(LocalDateTime.now())) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage(Error.EXPIRED.getErrorMessage(VerificationEntity.termSeconds))), HttpStatus.BAD_REQUEST);
        }

        //todo check verification code
        if (!passwordTemporary.getVerification().getCode().equals(verificationCode)) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage(Error.VERIFICATION_CODE_NOT_MATCH.getErrorMessage(verificationCode))), HttpStatus.BAD_REQUEST);
        }

        UserEntity user = passwordTemporary.getUser();
        user.setPassword(passwordTemporary.getNewPassword());
        userRepo.save(user);


        passwordTemporaryRepo.delete(passwordTemporary);
        verificationRepo.delete(passwordTemporary.getVerification());

        UserResponseDto userResponseDto = userService.toUserResponse(user);
        return new ResponseEntity<>(new ApiResponse<>(userResponseDto, new SuccessMessage()), HttpStatus.OK);
    }
}
