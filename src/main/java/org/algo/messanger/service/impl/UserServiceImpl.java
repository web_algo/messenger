package org.algo.messanger.service.impl;

import lombok.RequiredArgsConstructor;
import org.algo.messanger.common.Utils;
import org.algo.messanger.domain.PasswordTemporary;
import org.algo.messanger.domain.PhotoEntity;
import org.algo.messanger.domain.VerificationEntity;
import org.algo.messanger.domain.sittings.TwoStepAuth;
import org.algo.messanger.domain.user.TemporaryUserEntity;
import org.algo.messanger.domain.user.UserEntity;
import org.algo.messanger.enums.Error;
import org.algo.messanger.enums.Status;
import org.algo.messanger.enums.Success;
import org.algo.messanger.model.request.UserCreateDto;
import org.algo.messanger.model.request.UserUpdateDto;
import org.algo.messanger.model.response.PhotoResponseDto;
import org.algo.messanger.model.response.UserResponseDto;
import org.algo.messanger.model.response.VerificationCodeResponseDto;
import org.algo.messanger.model.response.template.ApiResponse;
import org.algo.messanger.model.response.template.ErrorMessage;
import org.algo.messanger.model.response.template.SuccessMessage;
import org.algo.messanger.repository.*;
import org.algo.messanger.service.UserService;
import org.algo.messanger.service.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {


    private final Utils utils;
    private final UserRepo userRepo;
    private final TemporaryRepo temporaryRepo;
    private final SittingsServiceImpl sittingsService;
    private final SittingsRepo sittingsRepo;
    private final NotificationSittingsRepo notificationSittingsRepo;
    private final PrivacySecuritySittingsRepo privacySecuritySittingsRepo;
    private final FolderSittingsRepo folderSittingsRepo;
    private final MyFolderRepo myFolderRepo;
    private final TwoStepAuthRepo twoStepAuthRepo;
    private final PasswordTemporaryRepo passwordTemporaryRepo;

    @Autowired
    VerificationService verificationService;


    @Override
    public ResponseEntity<ApiResponse<UserResponseDto>> get(Long userId) {
        Optional<UserEntity> optionalUser = userRepo.findById(userId);
        if (optionalUser.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage(Error.USER_NOT_FOUND.getErrorMessage(userId)), false), HttpStatus.NOT_FOUND);
        }

        UserResponseDto userResponseDto = toUserResponse(optionalUser.get());
        return new ResponseEntity<>(new ApiResponse<>(userResponseDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse<VerificationCodeResponseDto>> create(UserCreateDto userCreateDto) {

        Optional<UserEntity> optionalUser = userRepo.findByEmail(userCreateDto.getEmail());
        if (optionalUser.isPresent()) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage(Error.EMAIL_EXIST.getErrorMessage(userCreateDto.getEmail()))), HttpStatus.BAD_REQUEST);
        }

        optionalUser = userRepo.findByUsername(userCreateDto.getUsername());
        if (optionalUser.isPresent()) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage(Error.USERNAME_EXIST.getErrorMessage(userCreateDto.getUsername()))), HttpStatus.BAD_REQUEST);
        }

        TemporaryUserEntity temporaryUserEntity = new TemporaryUserEntity();
        temporaryUserEntity.setEmail(userCreateDto.getEmail());
        temporaryUserEntity.setUsername(userCreateDto.getUsername());
        temporaryUserEntity.setFirstName(userCreateDto.getFirstName());
        temporaryUserEntity.setLastName(userCreateDto.getLastName());
        temporaryUserEntity.setPassword(userCreateDto.getPassword());
        temporaryUserEntity.setVerification(verificationService.sendVerificationToken(userCreateDto.getEmail()));
        temporaryRepo.save(temporaryUserEntity);

        VerificationCodeResponseDto verifyCodeResponse = new VerificationCodeResponseDto(VerificationEntity.termSeconds);
        SuccessMessage successMessage = new SuccessMessage(Success.SUCCESSFULLY_SEND.getSuccessMessage(userCreateDto.getEmail()));
        return new ResponseEntity<>(new ApiResponse<>(verifyCodeResponse, successMessage), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse<UserResponseDto>> update(UserUpdateDto userUpdateDto) {

        Optional<UserEntity> optionalUser = userRepo.findById(userUpdateDto.getId());
        if (optionalUser.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage(Error.USER_NOT_FOUND.getErrorMessage(userUpdateDto.getId()))), HttpStatus.NOT_FOUND);
        }

        UserEntity user = optionalUser.get();

        user.setFirstName(utils.isEmptyString(userUpdateDto.getFirstName()) ? user.getFirstName() : userUpdateDto.getFirstName());
        user.setLastName(utils.isEmptyString(userUpdateDto.getLastName()) ? user.getLastName() : userUpdateDto.getLastName());
        user.setUsername(utils.isEmptyString(userUpdateDto.getUsername()) ? user.getUsername() : userUpdateDto.getUsername());
        user.setBio(utils.isEmptyString(userUpdateDto.getBio()) ? user.getBio() : userUpdateDto.getBio());


        user.setLastActivity(utils.isNull(userUpdateDto.getLastActivity()) ? user.getLastActivity() : userUpdateDto.getLastActivity());

        if (!utils.isEmptyString(userUpdateDto.getStatus())) {
            if (userUpdateDto.getStatus().equals(Status.ONLINE.name())) {
                user.setStatus(Status.ONLINE);
            } else if (userUpdateDto.getStatus().equals(Status.OFFLINE.name())) {
                user.setStatus(Status.OFFLINE);
            }
        }

        UserEntity userEntity = userRepo.save(user);
        UserResponseDto userResponseDto = toUserResponse(userEntity);

        return new ResponseEntity<>(new ApiResponse<>(userResponseDto, new SuccessMessage()), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse<Boolean>> delete(Long userId) {
        Optional<UserEntity> optionalUser = userRepo.findById(userId);
        if (optionalUser.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage()), HttpStatus.NOT_FOUND);
        }

        UserEntity user = optionalUser.get();
        userRepo.delete(user);
        sittingsRepo.delete(user.getSittingsEntity());

        folderSittingsRepo.delete(user.getSittingsEntity().getFolderSittings());
        myFolderRepo.deleteAll(user.getSittingsEntity().getFolderSittings().getMyFolderList());

        privacySecuritySittingsRepo.delete(user.getSittingsEntity().getPrivacySecuritySittings());
        twoStepAuthRepo.delete(user.getSittingsEntity().getPrivacySecuritySittings().getTwoStepAuth());

        notificationSittingsRepo.delete(user.getSittingsEntity().getNotificationSittings());


        return new ResponseEntity<>(new ApiResponse<>(true, new SuccessMessage()), HttpStatus.OK);


    }


    @Override
    public ResponseEntity<ApiResponse<Boolean>> changePassword(String newPassword, Long userId) {
        Optional<UserEntity> optionalUser = userRepo.findById(userId);
        if (optionalUser.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage(Error.USER_NOT_FOUND.getErrorMessage(userId))), HttpStatus.NOT_FOUND);
        }

        UserEntity user = optionalUser.get();

        VerificationEntity verification = verificationService.sendVerificationCode(user.getEmail());

        PasswordTemporary passwordTemporary = new PasswordTemporary();
        passwordTemporary.setEmail(user.getEmail());
        passwordTemporary.setNewPassword(newPassword);
        passwordTemporary.setUser(user);
        passwordTemporary.setVerification(verification);

        Optional<PasswordTemporary> optionalPasswordTemporary = passwordTemporaryRepo.findByEmail(user.getEmail());

        optionalPasswordTemporary.ifPresent(temporary -> passwordTemporary.setId(temporary.getId()));
        passwordTemporaryRepo.save(passwordTemporary);

        return new ResponseEntity<>(new ApiResponse<>(new SuccessMessage(Success.SUCCESSFULLY_SEND.getSuccessMessage(user.getEmail()))), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ApiResponse<Boolean>> twoStepAuth(String question, String answer, Boolean isActive, Long userId) {
        Optional<UserEntity> optionalUser = userRepo.findById(userId);
        if (optionalUser.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse<>(new ErrorMessage(Error.USER_NOT_FOUND.getErrorMessage(userId))), HttpStatus.NOT_FOUND);
        }

        TwoStepAuth twoStepAuth = optionalUser.get().getSittingsEntity().getPrivacySecuritySittings().getTwoStepAuth();

        twoStepAuth.setAuthActive(isActive);
        twoStepAuth.setQuestion(question);
        twoStepAuth.setAnswer(answer);
        twoStepAuthRepo.save(twoStepAuth);

        return new ResponseEntity<>(new ApiResponse<>(new SuccessMessage()), HttpStatus.OK);
    }

    @Override
    public UserEntity toUserEntity(TemporaryUserEntity temporary) {
        UserEntity user = new UserEntity();

        user.setFirstName(temporary.getFirstName());
        user.setLastName(temporary.getLastName());
        user.setUsername(temporary.getUsername());
        user.setEmail(temporary.getEmail());
        user.setPassword(temporary.getPassword());
        user.setBio("");
        user.setStatus(Status.OFFLINE);
        user.setLastActivity(LocalDateTime.now());
        List<PhotoEntity> photoEntityList = new ArrayList<>();
        user.setPhotoEntityList(photoEntityList);

        user.setSittingsEntity(sittingsService.getDefaultSittings());

        return userRepo.save(user);
    }

    @Override
    public UserResponseDto toUserResponse(UserEntity userEntity) {
        UserResponseDto userResponseDto = new UserResponseDto();
        userResponseDto.setUserId(userEntity.getId());
        userResponseDto.setFirstName(userEntity.getFirstName());
        userResponseDto.setLastName(userEntity.getLastName());
        userResponseDto.setUsername(userEntity.getUsername());
        userResponseDto.setEmail(userEntity.getEmail());
        userResponseDto.setBio(userEntity.getBio());
        userResponseDto.setLastActivity(userEntity.getLastActivity());
        userResponseDto.setStatus(userEntity.getStatus().name());
        List<PhotoResponseDto> photoResponseList = new ArrayList<>();

        for (PhotoEntity photoEntity : userEntity.getPhotoEntityList()) {
            PhotoResponseDto photoResponseDto = PhotoEntity.toPhotoResponse(photoEntity);
            photoResponseList.add(photoResponseDto);
        }

        userResponseDto.setPhotoResponseList(photoResponseList);
        return userResponseDto;
    }


}