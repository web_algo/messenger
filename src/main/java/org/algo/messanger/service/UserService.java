package org.algo.messanger.service;

import org.algo.messanger.domain.user.TemporaryUserEntity;
import org.algo.messanger.domain.user.UserEntity;
import org.algo.messanger.model.request.UserCreateDto;
import org.algo.messanger.model.request.UserUpdateDto;
import org.algo.messanger.model.response.UserResponseDto;
import org.algo.messanger.model.response.VerificationCodeResponseDto;
import org.algo.messanger.model.response.template.ApiResponse;
import org.springframework.http.ResponseEntity;

public interface UserService {
    ResponseEntity<ApiResponse<UserResponseDto>> get(Long userId);

    ResponseEntity<ApiResponse<VerificationCodeResponseDto>> create(UserCreateDto userCreateDto);

    ResponseEntity<ApiResponse<UserResponseDto>> update(UserUpdateDto userUpdateDto);

    ResponseEntity<ApiResponse<Boolean>> delete(Long userId);

    UserEntity toUserEntity(TemporaryUserEntity temporary);

    UserResponseDto toUserResponse(UserEntity userEntity);

    ResponseEntity<ApiResponse<Boolean>> changePassword(String newPassword, Long userId);

    ResponseEntity<ApiResponse<Boolean>> twoStepAuth(String question, String answer, Boolean isActive, Long userId);
}
