package org.algo.messanger.enums;

import lombok.Getter;

public enum ChatType {
    PRIVATE,
    GROUP,
    SUPERGROUP,
    CHANNEL
}
