package org.algo.messanger.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Error {
    USER_NOT_FOUND("user not fount -wrong userId:"),
    VERIFICATION_CODE_NOT_MATCH("verification code not match -wrong code:"),
    EMAIL_NOT_FOUND("email not fount -wrong email:"),
    EXPIRED("expired time -period seconds:"),
    EMAIL_EXIST("email already exist -wrong email:"),
    USERNAME_EXIST("username already exist -wrong username:");
    private String errorMessage;

    public String getErrorMessage(Long hint) {
        return errorMessage + hint;
    }

    public String getErrorMessage(Integer hint) {
        return errorMessage + hint;
    }

    public String getErrorMessage(String hint) {
        return errorMessage + hint;
    }
}
