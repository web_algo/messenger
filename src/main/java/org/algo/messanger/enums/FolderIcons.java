package org.algo.messanger.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FolderIcons {
    ICON_DEFAULT("-1"),
    ICON_ONE("1"),
    ICON_TWO("2"),
    ICON_THREE("3"),
    ICON_FOUR("4"),
    ICON_FIVE("5");
    private String code;
}
