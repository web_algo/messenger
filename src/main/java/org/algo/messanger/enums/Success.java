package org.algo.messanger.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Success {
    SUCCESSFULLY_SEND("verification code send -to email:");
    private String successMessage;

    public String getSuccessMessage(String email) {
        return SUCCESSFULLY_SEND.successMessage + email;
    }
}
