package org.algo.messanger.enums;

public enum Language {
    ENGLISH,
    RUSSIAN,
    UZBEK,
    ARABIC,
    INDIAN,
    CHINESE
}
