package org.algo.messanger.enums;

public enum AwayFor {
    MONTH,
    THREE_MONTH,
    SIX_MONTH,
    YEAR
}
