package org.algo.messanger.enums;

public enum MessageContentType {
    TEXT,
    VIDEO,
    PHOTO,
    VOICE
}
