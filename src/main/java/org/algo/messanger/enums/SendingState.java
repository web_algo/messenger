package org.algo.messanger.enums;

public enum SendingState {
    SUCCESSFUL,
    PENDING,
    FAIL
}
