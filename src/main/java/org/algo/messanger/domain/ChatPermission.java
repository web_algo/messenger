package org.algo.messanger.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ChatPermission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Boolean canAddWebPagePreviews;
    @Column
    private Boolean canChangeInfo;
    @Column
    private Boolean canInviteUsers;
    @Column
    private Boolean canPinMessages;
    @Column
    private Boolean canSendMediaMessages;
    @Column
    private Boolean canSendMessages;
    @Column
    private Boolean canSendOtherMessages;
}
