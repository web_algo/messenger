package org.algo.messanger.domain.sittings;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algo.messanger.enums.AwayFor;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PrivacySecuritySittingsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "boolean default true")
    private Boolean addMeToGroupsOrChannels;

    @Column(columnDefinition = "boolean default true")
    private Boolean showLastSeenOnline;

    @Column(columnDefinition = "boolean default true")
    private Boolean showPhone;

    @Column(columnDefinition = "boolean default true")
    private Boolean showProfilePhoto;

    @Column
    @Enumerated(EnumType.STRING)
    private AwayFor ifAwayFor;

    @OneToOne
    private TwoStepAuth twoStepAuth;


}
