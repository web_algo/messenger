package org.algo.messanger.domain.sittings;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algo.messanger.enums.Language;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SittingsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Language language;

    @OneToOne(optional = false)
    private FolderSittingsEntity folderSittings;

    @OneToOne(optional = false)
    private NotificationSittingsEntity notificationSittings;

    @OneToOne(optional = false)
    private PrivacySecuritySittingsEntity privacySecuritySittings;


}
