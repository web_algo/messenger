package org.algo.messanger.domain.sittings;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class NotificationSittingsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "boolean default true")
    private Boolean joinTelegram;

    @Column(columnDefinition = "boolean default true")
    private Boolean pinnedMessage;

    @Column(columnDefinition = "boolean default true")
    private Boolean playSound;

    @Column(columnDefinition = "boolean default true")
    private Boolean showPreviewMessage;

    @Column(columnDefinition = "boolean default true")
    private Boolean showSenderName;


}
