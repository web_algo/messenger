package org.algo.messanger.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algo.messanger.model.response.PhotoResponseDto;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PhotoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String pathId;

    @Column
    private String path;

    @Column
    private LocalDateTime uploadDate;


    public static PhotoResponseDto toPhotoResponse(PhotoEntity photoEntity) {
        PhotoResponseDto photoResponseDto = new PhotoResponseDto();
        photoResponseDto.setPathId(photoResponseDto.getPathId());
        photoResponseDto.setUploadDate(photoEntity.getUploadDate());
        return photoResponseDto;
    }

}
