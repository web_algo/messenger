package org.algo.messanger.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algo.messanger.domain.chat.Chat;
import org.algo.messanger.enums.FolderIcons;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MyFolder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, columnDefinition = "varchar default '-1'")
    @Enumerated(EnumType.STRING)
    private FolderIcons folderIcons;

    @Column(nullable = false, length = 15)
    private String folderName;

    @OneToMany
    private List<Chat> chatList;
}