package org.algo.messanger.domain.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algo.messanger.domain.PhotoEntity;
import org.algo.messanger.domain.sittings.SittingsEntity;
import org.algo.messanger.enums.Status;
import org.algo.messanger.model.response.PhotoResponseDto;
import org.algo.messanger.model.response.UserResponseDto;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 32, nullable = false)
    private String firstName;

    @Column(length = 32)
    private String lastName;

    @Column(length = 150)
    private String bio;

    @Column(length = 64, nullable = false, unique = true)
    private String username;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column
    private LocalDateTime lastActivity;

    @Column
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToOne
    private SittingsEntity sittingsEntity;

    @OneToMany
    private List<PhotoEntity> photoEntityList;





}
