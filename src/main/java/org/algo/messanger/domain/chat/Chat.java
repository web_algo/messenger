package org.algo.messanger.domain.chat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algo.messanger.domain.ChatPermission;
import org.algo.messanger.domain.MessageEntity;
import org.algo.messanger.domain.user.UserEntity;
import org.algo.messanger.enums.ChatType;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private UserEntity user1;

    @OneToOne
    private UserEntity user2;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ChatType chatType;

    @Column(columnDefinition = "integer default 0")
    private Integer unreadCount;

    @OneToOne(optional = false)
    private ChatPermission chatPermission;

    @OneToMany
    private List<MessageEntity> messageList;
}
