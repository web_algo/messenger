package org.algo.messanger.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algo.messanger.domain.user.UserEntity;
import org.algo.messanger.enums.SendingState;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MessageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, columnDefinition = "timestamp default now()")
    private LocalDateTime sendingDate;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private SendingState sendingState;

    @OneToOne(optional = false)
    private MessageContent messageContent;

    @OneToOne(optional = false)
    private UserEntity sender;
}