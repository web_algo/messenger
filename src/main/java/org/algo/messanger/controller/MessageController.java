package org.algo.messanger.controller;

import lombok.RequiredArgsConstructor;
import org.algo.messanger.model.response.MessageResponseDto;
import org.algo.messanger.model.response.template.ApiResponse;
import org.algo.messanger.service.MessageService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
@RequiredArgsConstructor
public class MessageController {

    private final MessageService messageService;

    @GetMapping("message")
    public ResponseEntity<ApiResponse<MessageResponseDto>> getMessage(@RequestParam Long id){
        return messageService.getMessage(id);
    }

}
