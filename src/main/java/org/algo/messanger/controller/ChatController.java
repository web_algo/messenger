package org.algo.messanger.controller;

import lombok.RequiredArgsConstructor;
import org.algo.messanger.model.request.ChatPermissionDto;
import org.algo.messanger.model.response.ChatResponseDto;
import org.algo.messanger.model.response.MessageResponseDto;
import org.algo.messanger.model.response.template.ApiResponse;
import org.algo.messanger.model.response.template.ListPage;
import org.algo.messanger.service.ChatService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
@RequiredArgsConstructor
public class ChatController {

    private final ChatService chatService;

    @GetMapping("chat")
    public ResponseEntity<ApiResponse<ChatResponseDto>> getChat(@RequestParam Long chatId) {
        return chatService.get(chatId);
    }

    @PutMapping("chat/permissions")
    public ResponseEntity<ApiResponse<ChatResponseDto>> putChat(@RequestBody ChatPermissionDto chatPermissionDto, @RequestParam Long chatId) {
        return chatService.update(chatPermissionDto, chatId);
    }

    @GetMapping("chat/messages")
    public ResponseEntity<ApiResponse<ListPage<MessageResponseDto>>> getMessages(@RequestParam Long start, @RequestParam Integer limit, @RequestParam Long chatId) {
        return chatService.getMessages(start, limit, chatId);
    }

    @PostMapping("chat/create")
    public ResponseEntity<ApiResponse<ChatResponseDto>> createChat(@RequestParam Long myUserId, @RequestParam String interlocutorUsername) {
        return chatService.create(myUserId, interlocutorUsername);
    }

}
