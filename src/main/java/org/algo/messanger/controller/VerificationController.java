package org.algo.messanger.controller;

import lombok.RequiredArgsConstructor;
import org.algo.messanger.model.response.UserResponseDto;
import org.algo.messanger.model.response.template.ApiResponse;
import org.algo.messanger.service.VerificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
@RequiredArgsConstructor
public class VerificationController {

    private final VerificationService verificationService;

    @GetMapping("/verify/token")
    public ResponseEntity<ApiResponse<UserResponseDto>> verifyWithToken(@RequestParam String email, @RequestParam String verificationCode) {
        return verificationService.verifyEmail(email, verificationCode);
    }

    @GetMapping("verify/code")
    public ResponseEntity<ApiResponse<UserResponseDto>> verifyWithCode(@RequestParam String email, @RequestParam String verificationCode) {
        return verificationService.verifyNewPassword(email, verificationCode);
    }
}
