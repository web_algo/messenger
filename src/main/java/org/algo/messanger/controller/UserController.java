package org.algo.messanger.controller;

import lombok.RequiredArgsConstructor;
import org.algo.messanger.model.request.UserCreateDto;
import org.algo.messanger.model.request.UserUpdateDto;
import org.algo.messanger.model.response.UserResponseDto;
import org.algo.messanger.model.response.VerificationCodeResponseDto;
import org.algo.messanger.model.response.template.ApiResponse;
import org.algo.messanger.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;


    @GetMapping("user")
    public ResponseEntity<ApiResponse<UserResponseDto>> getUser(@RequestParam Long userId) {
        return userService.get(userId);
    }


    @PostMapping("user")
    public ResponseEntity<ApiResponse<VerificationCodeResponseDto>> postUser(@Valid @RequestBody UserCreateDto userCreateDto) {
        return userService.create(userCreateDto);
    }

    @PutMapping("user")
    public ResponseEntity<ApiResponse<UserResponseDto>> putUser(@RequestBody UserUpdateDto userUpdateDto) {
        return userService.update(userUpdateDto);
    }

    @DeleteMapping("user")
    public ResponseEntity<ApiResponse<Boolean>> deleteUser(@RequestParam Long userId) {
        return userService.delete(userId);
    }


    @PutMapping("user/password")
    public ResponseEntity<ApiResponse<Boolean>> changePassword(String newPassword, @RequestParam Long userId) {
        return userService.changePassword(newPassword, userId);
    }

    @PutMapping("user/twoStepAuthorization")
    public ResponseEntity<ApiResponse<Boolean>> turnOn(@RequestParam String question, @RequestParam String answer, @RequestParam Boolean isActive, @RequestParam Long userId) {
        return userService.twoStepAuth(question, answer, isActive, userId);
    }
}
