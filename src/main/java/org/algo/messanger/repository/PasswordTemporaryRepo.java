package org.algo.messanger.repository;

import org.algo.messanger.domain.PasswordTemporary;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PasswordTemporaryRepo extends JpaRepository<PasswordTemporary,Long> {

    Optional<PasswordTemporary> findByEmail(String email);
}
