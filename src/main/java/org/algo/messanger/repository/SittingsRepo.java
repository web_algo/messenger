package org.algo.messanger.repository;

import org.algo.messanger.domain.sittings.SittingsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SittingsRepo extends JpaRepository<SittingsEntity, Long> {
}
