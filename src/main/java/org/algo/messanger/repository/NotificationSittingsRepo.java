package org.algo.messanger.repository;

import org.algo.messanger.domain.sittings.NotificationSittingsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationSittingsRepo extends JpaRepository<NotificationSittingsEntity, Long> {
}
