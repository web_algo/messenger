package org.algo.messanger.repository;

import org.algo.messanger.domain.VerificationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VerificationRepo extends JpaRepository<VerificationEntity, Long> {

    Optional<VerificationEntity> findByEmail(String email);

    Optional<VerificationEntity> findByCode(String code);
}
