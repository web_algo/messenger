package org.algo.messanger.repository;

import org.algo.messanger.domain.PhotoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhotoRepo extends JpaRepository<PhotoEntity, Long> {
}
