package org.algo.messanger.repository;

import org.algo.messanger.domain.sittings.PrivacySecuritySittingsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrivacySecuritySittingsRepo extends JpaRepository<PrivacySecuritySittingsEntity, Long> {
}
