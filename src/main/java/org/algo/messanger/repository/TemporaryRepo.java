package org.algo.messanger.repository;

import org.algo.messanger.domain.user.TemporaryUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TemporaryRepo extends JpaRepository<TemporaryUserEntity, Long> {

    Optional<TemporaryUserEntity> findByVerificationId(Long verification_id);

    Optional<TemporaryUserEntity> findByEmail(String email);
}
