package org.algo.messanger.repository;

import org.algo.messanger.domain.MyFolder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MyFolderRepo extends JpaRepository<MyFolder, Long> {
}
