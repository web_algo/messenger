package org.algo.messanger.repository;

import org.algo.messanger.domain.ChatPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChatPermissionRepo extends JpaRepository<ChatPermission,Long> {
}
