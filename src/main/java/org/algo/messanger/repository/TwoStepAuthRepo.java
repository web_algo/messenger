package org.algo.messanger.repository;

import org.algo.messanger.domain.sittings.TwoStepAuth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TwoStepAuthRepo extends JpaRepository<TwoStepAuth, Long> {
}
