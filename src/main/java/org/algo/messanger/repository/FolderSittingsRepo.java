package org.algo.messanger.repository;

import org.algo.messanger.domain.sittings.FolderSittingsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FolderSittingsRepo extends JpaRepository<FolderSittingsEntity, Long> {
}
