package org.algo.messanger.repository;

import org.algo.messanger.domain.MessageContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageContentRepo extends JpaRepository<MessageContent, Long> {
}
