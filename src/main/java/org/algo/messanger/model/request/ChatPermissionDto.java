package org.algo.messanger.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChatPermissionDto {

    private Boolean canAddWebPagePreviews;

    private Boolean canChangeInfo;

    private Boolean canInviteUsers;

    private Boolean canPinMessages;

    private Boolean canSendMediaMessages;

    private Boolean canSendMessages;

    private Boolean canSendOtherMessages;
}
