package org.algo.messanger.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateDto {

    private String firstName;

    private String lastName;

    private String username;

    private String email;

    @Size(min = 8, message = "The minimum length of the password must be 8")
    private String password;
}
