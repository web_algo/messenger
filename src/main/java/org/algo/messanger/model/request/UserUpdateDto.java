package org.algo.messanger.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class UserUpdateDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String username;

    private String bio;

    private LocalDateTime lastActivity;

    private String status;
}
