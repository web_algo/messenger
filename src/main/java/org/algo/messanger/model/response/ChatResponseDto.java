package org.algo.messanger.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algo.messanger.enums.ChatType;
import org.algo.messanger.model.request.ChatPermissionDto;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChatResponseDto {

    private Long chatId;

    private ChatType chatType;

    private Integer unreadCount;

    private ChatPermissionDto chatPermission;

    private List<MessageResponseDto> lastHundredMessages;

    private Long allMessagesCount;
}
