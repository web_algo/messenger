package org.algo.messanger.model.response.template;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class ListPage<T> {

    private List<T> list;

    private Integer size;

    private Long start;
}
