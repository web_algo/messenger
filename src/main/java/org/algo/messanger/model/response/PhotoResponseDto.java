package org.algo.messanger.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class PhotoResponseDto {

    private String pathId;

    private LocalDateTime uploadDate;
}
