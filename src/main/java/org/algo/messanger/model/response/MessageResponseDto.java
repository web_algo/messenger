package org.algo.messanger.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algo.messanger.domain.MessageContent;
import org.algo.messanger.enums.SendingState;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MessageResponseDto {

    private Long id;

    private LocalDateTime sendingDate;

    private SendingState sendingState;

    private MessageContent messageContent;

    private UserResponseDto sender;
}
