package org.algo.messanger.model.response.template;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse<T> {
    private T data;
    private ResponseMessage responseMessage;
    private boolean success;

    public ApiResponse(T data) {
        this.data = data;
        this.success = true;
        this.responseMessage = new SuccessMessage("");
    }

    public ApiResponse(T data, boolean success) {
        this.data = data;
        if (success) {
            this.responseMessage = new SuccessMessage("");
        } else {
            this.responseMessage = new ErrorMessage("");
        }
        this.success = success;
    }

    public ApiResponse(boolean success) {
        if (success) {
            this.responseMessage = new SuccessMessage("");
        } else {
            this.responseMessage = new ErrorMessage("");
        }
        this.success = success;
    }

    public ApiResponse(ResponseMessage responseMessage, boolean success) {
        this.responseMessage = responseMessage;
        this.success = success;
    }

    public ApiResponse(ResponseMessage responseMessage) {
        this.data = null;
        this.success = responseMessage.getClass().equals(SuccessMessage.class);
        this.responseMessage = responseMessage;
    }

    public ApiResponse(T data,ResponseMessage responseMessage) {
        this.data = data;
        this.success = responseMessage.getClass().equals(SuccessMessage.class);
        this.responseMessage = responseMessage;
    }
}
