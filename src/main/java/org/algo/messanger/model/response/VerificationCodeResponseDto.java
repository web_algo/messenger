package org.algo.messanger.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class VerificationCodeResponseDto {

    private Integer termTimeInSeconds;

}
