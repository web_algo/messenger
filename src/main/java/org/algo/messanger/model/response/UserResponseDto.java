package org.algo.messanger.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algo.messanger.enums.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class UserResponseDto {

    private Long userId;

    private String firstName;

    private String lastName;

    private String bio;

    private String username;

    private String email;

    private LocalDateTime lastActivity;

    private String status;

    private List<PhotoResponseDto> photoResponseList;
}
