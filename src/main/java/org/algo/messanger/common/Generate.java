package org.algo.messanger.common;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

public class Generate {

    /**
     * Generate a random string.
     */

    public String nextString() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

    public static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String lower = upper.toLowerCase(Locale.ROOT);

    public static final String digits = "0123456789";

    public static final String alphabetic = upper+lower;
    public static final String special = "@#%$&_=+-.:!,;*";

    public static final String alphanum = upper + lower + digits;
    public static final String specialalphanum = upper + lower + digits+special;

    private final Random random;

    private final char[] symbols;

    private final char[] buf;

    public Generate(int length, Random random, String symbols) {
        if (length < 1) throw new IllegalArgumentException();
        if (symbols.length() < 2) throw new IllegalArgumentException();
        this.random = Objects.requireNonNull(random);
        this.symbols = symbols.toCharArray();
        this.buf = new char[length];
    }

    /**
     * Create an alphanumeric string generator.
     */
    public Generate(int length, Random random) {
        this(length, random, alphanum);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     */
    public Generate(int length) {
        this(length, new SecureRandom());
    }

    /**
     * Create session identifiers.
     */
    public Generate() {
        this(21);
    }

}