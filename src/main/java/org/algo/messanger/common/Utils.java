package org.algo.messanger.common;

import org.springframework.stereotype.Component;

@Component
public class Utils {
    public boolean isEmptyString(String value) {
        if (value == null) return true;
        return value.trim().length() == 0;
    }

    public boolean isNull(Object object) {
        return object == null;
    }
}
